package logic;

import java.util.Arrays;
import java.util.List;

public class Calculator {
    public double calculate(String expression) {
        expression = expression.replaceAll("\\s+", "");
        if (!isValidExpression(expression)) {
            throw  new RuntimeException();
        }
        return Double.parseDouble(operate(expression));
    }
    private String operate(String expression) {
        String operationResult;
        List <String> listOfArguments = Arrays.stream(expression.split("(?<=[-+*/()])|(?=[-+*/()])")).toList();
        Calculation calculation = new Calculation(listOfArguments);
        operationResult = calculation.getResult();
        return operationResult;
    }
    private boolean isValidExpression(String expression) {
        for (int i = 0; i < expression.length(); i++) {
            char currentChar = expression.charAt(i);
            if (!isNumberOrOperator(currentChar)){
                return false;
            }
        }
        return true;
    }
    private boolean isNumberOrOperator(char currentChar) {
        return Character.isDigit(currentChar) || currentChar == '+'
                || currentChar == '-' || currentChar == '*' || currentChar == '/'
                || currentChar == '(' || currentChar == ')';
    }
}
