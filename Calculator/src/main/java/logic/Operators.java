package logic;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Operators {
    private final DecimalFormat decimalFormat = new DecimalFormat("0.00",  new DecimalFormatSymbols(Locale.US));

    public String add(String number1, String number2) {
        double result = Double.parseDouble(number1) + Double.parseDouble(number2);
        return String.valueOf(decimalFormat.format(result));
    }

    public String decrease(String minuend, String subtrahend) {
        double result = Double.parseDouble(minuend) - Double.parseDouble(subtrahend);
        return String.valueOf(decimalFormat.format(result));
    }

    public String multiply(String factor1, String factor2) {
        double result = Double.parseDouble(factor1) * Double.parseDouble(factor2);
        return String.valueOf(decimalFormat.format(result));
    }

    public String divide(String dividend, String divisor) {
        double result = Double.parseDouble(dividend) / Double.parseDouble(divisor);
        return String.valueOf(decimalFormat.format(result));
    }

}
