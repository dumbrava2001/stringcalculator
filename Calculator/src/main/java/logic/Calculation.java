package logic;

import java.util.ArrayList;
import java.util.List;

public class Calculation {
    private final List<String> expressionArguments;
    private final Operators operators;
    public Calculation(List<String> expressionArguments){
        this.expressionArguments = new ArrayList<>(expressionArguments);
        this.operators = new Operators();
    }
    public String getResult(){
        while (isAnExpression(expressionArguments)){
            if (isExpressionWithBrackets(expressionArguments)) {
                calculateExpressionWithBrackets();
            }else{
                calculateExpression(expressionArguments);
            }
        }
        return expressionArguments.get(0);
    }
    private void calculateExpressionWithBrackets(){
        int openBracketIndex = expressionArguments.lastIndexOf("(");
        int closeBracketIndex = 0;
        List<String> expressionFromBrackets = new ArrayList<>();
        for (int i = openBracketIndex + 1; i < expressionArguments.size(); i++) {
            if (expressionArguments.get(i).equals(")")) {
                closeBracketIndex = i;
                break;
            }
            expressionFromBrackets.add(expressionArguments.get(i));
        }
        while (isAnExpression(expressionFromBrackets)) {
            calculateExpression(expressionFromBrackets);
        }
        for (int i = openBracketIndex; i < closeBracketIndex; i++) {
            expressionArguments.remove(openBracketIndex+1);
        }
        String result = expressionFromBrackets.get(0);
        expressionArguments.set(openBracketIndex, result);
    }
    private void calculateExpression(List<String> expression) {
        int indexOfOperators = getIndexOfOperatorFromExpression(expression);
        String result = resultOfOperation(expression,indexOfOperators);
        deleteArgumentsByOperator(expression,indexOfOperators, result);
    }
    private int getIndexOfOperatorFromExpression(List<String> expression) {
        int index = -1;
        if (expression.contains("*") || expression.contains("/")) {
            for (String operator : expression) {
                if (operator.equals("*") || operator.equals("/")) {
                    index =  expression.indexOf(operator);
                    break;
                }
            }
        }else {
            for (String operator : expression) {
                if (operator.equals("+") || operator.equals("-")) {
                    index =  expression.indexOf(operator);
                    break;
                }
            }
        }
        return index;
    }
    private String resultOfOperation(List<String> expression, int indexOfOperator) {
        String operator = expression.get(indexOfOperator);
        return switch (operator) {
            case "*" -> operators.multiply(expression.get(indexOfOperator - 1),
                    expression.get(indexOfOperator + 1));
            case "/" -> operators.divide(expression.get(indexOfOperator - 1),
                    expression.get(indexOfOperator + 1));
            case "+" -> operators.add(expression.get(indexOfOperator - 1),
                    expression.get(indexOfOperator + 1));
            default -> operators.decrease(expression.get(indexOfOperator - 1),
                    expression.get(indexOfOperator + 1));
        };
    }
    private boolean isAnExpression(List<String> expression) {
        return expression.contains("+") || expression.contains("-") ||
                expression.contains("*") || expression.contains("/");
    }
    private boolean isExpressionWithBrackets(List<String> expression){
        return expression.contains("(") || expression.contains(")");
    }
    private void deleteArgumentsByOperator(List<String> expression,int indexOfOperation, String newResult) {
        expression.remove(indexOfOperation);
        expression.remove(indexOfOperation);
        expression.set(indexOfOperation - 1, newResult);
    }


}
