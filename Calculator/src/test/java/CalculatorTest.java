import logic.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {
    private final Calculator calculator = new Calculator();
    private String expression;

    @Test
    public void testingInvalidExpression(){
        expression = "1 + b - 10^2";
        assertThrows(RuntimeException.class, () -> calculator.calculate(expression));
    }
    @Test
    public void testingOnlyAddingAndDecreasing1(){
        expression = "1 + 2 - 5 + 6 - 7";
        assertEquals(-3.0, calculator.calculate(expression));
    }

    @Test
    public void testingOnlyAddingAndDecreasing2(){
        expression = "0 - 5 + 4 - 14 + 32";
        assertEquals(17, calculator.calculate(expression));
    }

    @Test
    public void testingOnlyMultiplyAndDivide1(){
        expression = "5 * 6 / 3 * 2 / 10";
        assertEquals(2, calculator.calculate(expression));
    }

    @Test
    public void testingOnlyMultiplyAndDivide2(){
        expression = "5 * 17 / 3 / 10";
        assertEquals(2.83, calculator.calculate(expression));
    }

    @Test
    public void testingWithNoParentheses1(){
        expression = "5 + 17 / 3 - 10 * 6";
        assertEquals(-49.33, calculator.calculate(expression));
    }

    @Test
    public void testingWithNoParentheses2(){
        expression = "5 * 17 / 3 - 10 + 6 - 5 * 3";
        assertEquals(9.33, calculator.calculate(expression));
    }

    @Test
    public void testingWithOnePairBrackets(){
        expression = "1+(2-6)/4";
        assertEquals(0, calculator.calculate(expression));
    }

    @Test
    public void testingWithTwoPairBrackets(){
        expression = "1+(2+6)/4*(1+2)";
        assertEquals(7, calculator.calculate(expression));
    }

    @Test
    public void testingWithThreePairBrackets(){
        expression = "(7-9)*1+(2+6)/4*(1+2)";
        assertEquals(4, calculator.calculate(expression));
    }

    @Test
    public void testingIndentedBrackets(){
        expression = "2*(3+(2-1*(4+9))/2)";
        assertEquals(-5, calculator.calculate(expression));
    }
}
